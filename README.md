<!--
vim: spelllang=fr keymap=accents
-->
# Exercice 5 de programmation pour STM32 : Communication I2C

Le protocole I2C est un protocole de communication synchrone qu'on considère
pour nos robots.

## I2C crash course

1. I2C utilise seulement 2 fils pour les signaux: un pour les données et un pour
	 l'horloge.
1. Le "contrôleur" I2C peux adresser jusqu'à (virtuellement) 127 "travailleurs"
	 (périphériques).
1. I2C n'est pas conçu pour des grandes distances et peut être très difficile à
	 faire fonctionner.

## Marches à suivre

### Téléchargement
Exécutez les commandes suivantes dans *Git Bash*.
1. Créez un dossier pour le répertoire:
	```
	mkdir -p ~/code/grum/eurobot2019/exercices/exercices-pour-stm32
	```
1. Clonez le répertoire:
	```
	git clone ssh://gitlab@gitlab.robitaille.host:49/GRUM/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-5-communication-i2c.git ~/code/grum/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-5-communication-i2c
	```
1. Ouvrez le dossier:
	```
	cd ~/code/GRUM/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-5-communication-i2c
	```
1. Ouvrez le dossier dans *Visual Sutdio Code*:
	```
	code .
	```

### Programmation
Ouvrez le fichier `src/main.cpp` et suivez les directives.

### Compilation
Une fois que le code est écrit, compiler le programme avec la commande
`PlatformIO: Build` (tapez <kbd>ctrl-shift-p</kbd> pour ouvrir la ligne de
commande) ou avec le raccourci <kbd>ctrl-alt-b</kbd>.

### Téléversement
Si vous avez accès à un microcontrôleur (on en a commandé!), téléversez le
programme compilé avec la commande `PlatformIO: Upload` (tapez
<kbd>ctrl-shift-p</kbd> pour ouvrir la ligne de commande) ou avec le raccourci
<kbd>ctrl-alt-u</kbd>.
