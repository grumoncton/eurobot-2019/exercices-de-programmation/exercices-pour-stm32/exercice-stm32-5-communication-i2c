#include <Arduino.h>
#include <Wire.h>
;

char message[64];
bool received = false;
bool requested = false;

void i2cHandler(int numBytes) {
	received = true;
	memset(message, 0, sizeof(message));

	for (uint8_t i = 0; i < numBytes; i++) {
		message[i] = Wire.read();
	}
}

void setup() {
	Serial.begin(9600);

	Wire.begin(0x27);
	Wire.onReceive(i2cHandler);
	Serial.println("Setup done");
}

void loop() {
	Serial.println(message);
}
